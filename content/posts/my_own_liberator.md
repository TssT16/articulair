---
title: My Own Liberator
date: 2020-06-01
imgw: https://a.wattpad.com/cover/71539132-176-k799434.jpg
categories: [one, two]
author: "Articular"
---
*Wir hätten es fast überstanden. Fast wärst du Mein gewesen, fast hätten wir unser Leben geteilt, fast hätte ich dich festhalten können. Und nun bricht dieses 'Fast' für immer mein Herz.*

Die Geschichte eines amerikanischen Soldaten, der während des zweiten Weltkrieges in Kontakt mit einem deutschen Mädchen kam.
Sie waren verschieden, die Welt, in der sie lebten grauenvoll und düster. 
Doch ihre Geschichte steht geschrieben.

written by @articulair  
Cover by @Little_Opheliae

[Auf wattpad lesen.](https://www.wattpad.com/255504838)